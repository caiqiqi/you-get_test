#!/usr/bin/env python3
import re

def match1(text, *patterns):
    """
    Scans through a string for substrings that matches some patterns (first-subgroups only).

    Args:
        text: The string to be scanned.
        patterns: Arbitrary number of regex patterns.

    Returns:
        When only one pattern is given, returns a string (None if no match found).
        When more than one pattern are given, returns a list of strings (return [] if no match found).
    """

    if len(patterns) == 1:
        pattern = patterns[0]
        match = re.search(pattern, test)
        if match:
            return match.group[1]
        else:
            return None

    else:
        ret = []
        for pattern in patterns:
            match = re.search(pattern, text)
            if match:
                ret.append(match.group(1))
        return ret
