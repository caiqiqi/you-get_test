#coding=utf-8
#!/usr/bin/env python3
#
# source : https://github.com/soimort/you-get/blob/develop/src/you_get/extractors/youku.py

from ..extractor import VideoExtractor

class Youku(VideoExtractor):
    name = "优酷 (Youku)"
    stream_types = [
        {'id': 'mp4hd3', 'alias-of' : 'hd3'},
        {'id': 'hd3',    'container': 'flv', 'video_profile': '1080P'},
        {'id': 'mp4hd2', 'alias-of' : 'hd2'},
        {'id': 'hd2',    'container': 'flv', 'video_profile': '超清'},
        {'id': 'mp4hd',  'alias-of' : 'mp4'},
        {'id': 'mp4',    'container': 'mp4', 'video_profile': '高清'},
        {'id': 'flvhd',  'container': 'flv', 'video_profile': '标清'},
        {'id': 'flv',    'container': 'flv', 'video_profile': '标清'},
        {'id': '3gphd',  'container': '3gp', 'video_profile': '标清（3GP）'},
    ]

    f_code_1 = 'becaf9be'
    f_code_2 = 'bf7e5f01'

    ctype = 12  #differ from 86

    def get_vid_from_url(url):
        """
        Extracts video ID from URL.
        """
        # 正则1 表示以"youku.com/id_"开头，然后接上a-zA-Z0-9=这这些字符中的至少一次
        # 正则2 表示以"player.youku.com.com/player.php/sid/([a-zA-Z0-9=]+)/v.swf" 这是一个flash文件
        # 正则3 表示以"loader.swf\?VideoIDS=([a-zA-Z0-9=]+)"  不需要优酷的域名都可以吗
        # 正则4 表示以"player.youku.com/embed/([a-zA-Z0-9=]+)" 匹配的嵌入了优酷播放器的网页吧
        return match1(url, r'youku\.com/v_show/id_([a-zA-Z0-9=]+)') or \
          match1(url, r'player\.youku\.com/player\.php/sid/([a-zA-Z0-9=]+)/v\.swf') or \
          match1(url, r'loader\.swf\?VideoIDS=([a-zA-Z0-9=]+)') or \
          match1(url, r'player\.youku\.com/embed/([a-zA-Z0-9=]+)')

    # 跟之前那个一样，只要你传给这个函数url参数了，就把这个对象的url变量更新一下
    def download_playlist_by_url(self, url, **kwargs):
        self.url = url

        try:
            playlist_id = self.__class__.get_playlist_id_from_url(self.url)

    def download_playlist_by_url(self, url, **kargs):
