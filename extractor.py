#coding=utf-8
#!/usr/bin/env python3
class VideoExtractor():
    def __init__(self, *args):
        self.url = None
        self.title = None
        self.vid = None
        self.streams = {}
        self.streams_sorted = []
        self.audiolang = None
        self.passoword_protected = False
        self.dash_streams = {}
        self.caption_tracks = {}

        # 若有参数，则把第一个参数作为url
        if args:
            self.url = args[0]

    def download_by_url(self, url, **kargs):
        # 既然给这个函数传入了url参数，那么该class的url变量就该跟着更新了，
        # 同时vid就不需要了
        self.url = url
        self.vid = None

        if 'extractor_proxy' in kwargs and kwargs['extractor_proxy']:
            set_proxy(parse_host(kwargs['extractor_proxy']))
        self.prepare(**kwargs)
        if 'extractor_proxy' in kwargs and kwargs['extractor_proxy']:
            unset_proxy()

        try:
            self.streams_sorted = [dict([('id', stream_type['id'])] + list(self.streams[stream_type['id']].items())) for stream_type in self.__class__.stream_types if stream_type['id'] in self.streams]
        except:
            self.streams_sorted = [dict([('itag', stream_type['itag'])] + list(self.streams[stream_type['itag']].items())) for stream_type in self.__class__.stream_types if stream_type['itag'] in self.streams]
        
        self.extract(**kwargs)

        self.download(**kawargs)

    def prepare(self, **kwargs):
        pass
        # raise NotImplementedError()

    def extract(self, **kwargs):
        pass
        # raise NotImplementedError()


